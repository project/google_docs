<?php

/**
 * @file
 * The google_docs module, theme functions.
 */

function theme_google_docs_doclist($documents, $total_results, $page=FALSE) {

  if ($page) {
    drupal_add_css(drupal_get_path('module', 'google_docs') .'/google_docs.css');
  }
  
  global $base_path;
  $tpath = drupal_get_path('module', 'google_docs') . "/icons";
  $ipath = $base_path . $tpath;
  
  $items = array();

  if ($page) {
    $logo = theme('image', "{$tpath}/gdocs_text_16.png", t('Google Docs logo'), t('Google Docs logo'), array('class' => 'gdocs-text'));
    $text = t('These documents are hosted on <a href="@gurl">!logo</a> and are not present on this site. Documents authors are therefore not actual users of this site. To edit, view or download any of these documents, you need <strong>your Google account</strong> to be given access to them by one of the document editor.', array('!logo' => $logo, '@gurl' => "http://docs.google.com"));
    $output =<<< EOHTML
      <div class="gdocs-heading">
      <a href="http://docs.google.com"><img class='icon' src='{$ipath}/gdocs_64.png' /></a>{$text}
      </div>
EOHTML;
  }
  
  if (!$documents) {
    $output .= "<p>" . t('No document present.') . "</p>";
  }
  else {
  
    $icosize = ($page ? '32' : '16');
    
    $up_txt = ($page ? t('Updated on') : '');
    $by_txt = t('by');
    $h = ($page ? "h3" : "h3");
    
    foreach ($documents as $doc) {
      $date = format_date($doc['updated'], 'small');
      $edit = l(t('View / Edit'), $doc['links']['edit']);
      $kind = $doc['categories']['kind'];

      $download = '';
      if ($page && ($format = google_docs_get_export_formats($kind))) {
        switch ($kind) {
          case 'spreadsheet':
            $base = GOOGLE_DOCS_SPREADSHEET_EXPORT_URI;
            $idkey = 'key';
            $more_query = "exportFormat=%f";
            break;

          case 'drawing':
            $base = GOOGLE_DOCS_DOCUMENT_EXPORT_URI;
            $idkey = 'docID';
            $more_query = "exportFormat=%f";
            break;
            
          case 'pdf':
            $base = GOOGLE_DOCS_PDF_EXPORT_URI;
            $idkey = 'id';
            $more_query = "export=download";
            break;
            
          default:
            $base = GOOGLE_DOCS_DOCUMENT_EXPORT_URI;
            $idkey = 'docID';
            $more_query = "exportFormat=%f&format=%f";
            break;
        }
        $download = '&ndash; ' . t('Download');
        foreach (google_docs_get_export_formats($kind) as $format) {
          $expq = str_replace('%f', $format, $more_query);
          $download .= ' ' . l($format, str_replace('%s', $kind, $base), array('attributes' => array('class' => 'gdocs-download new-window', 'target' => '_blank'), 'query' => "{$idkey}=" . urlencode($doc['id']) . "&{$expq}"));
        }
      }
      
      $items[] = "<{$h} class='title'><img title='{$kind}' alt='{$kind}' class='gdocs-icon' src=\"{$ipath}/gdoc_{$kind}_{$icosize}.png\"/> {$doc['title']}</{$h}>" . "<p class='metadata'>{$up_txt} {$date} {$by_txt} <strong>{$doc['updatedBy']}</strong> &ndash; {$edit} {$download}</p>";
    }
    
    $output .= theme('item_list', $items, NULL, 'ul', array('class' => 'gdocs-list'));
    
    if (!$page) {
      $ctx = _google_docs_get_context();
      $output .= "<div class=\"more-link\">" . l(t('Show all'), "google_docs/{$ctx->nid}/doclist") . "</div>";
    }
  }
  
  if ($page) {
    $limit = variable_get('gdocs_max_page_results', GOOGLE_DOCS_DEF_PAGE_RESULTS);
    if ($limit && $total_results > $limit) {
      $output .= "<p class='gdocs-stats'>" . t('Total !limit documents out of !total listed.', array('!total' => $total_results, '!limit' => $limit)) . "</p>";
    }
  }
  
  return $output;
}
