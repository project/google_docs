Google Docs integration module

DESCRIPTION
-----------

This module implements part of the Google Documents List Data API v2.0, as per
http://code.google.com/apis/documents/docs/2.0/developers_guide_protocol.html

The current version supports browsing documents for users (no folders, though).

It does not implements updating, creating, moving or trashing.

The primary purpose of this module is to offer a way for people to have their
document list visible on the site, with shortcut links for editing and downloading
them (if they can). The list is sorted by the last updated date.

The module integrates OG so that every group could have a specific google account
defined to browse documents from. Every group with a linked google account will
sport a new "Google Docs" tab.

INSTALLATION
------------

As usual, download the module and install it (a new database table will be created).

The "global" google account you can configure in the module settings page (optional),
is the account that will be used for the whole site (this google account will be 
the one linked to the document list for the *whole* site). Remember: if your site 
uses OG, you probably won't set this account. If you set up this account, you'll 
access its documents list from /google_docs/0/doclist URL. 

To setup an account for a group, simply edit the group page and add the google
account credentials.

SECURITY
--------

This module uses two way key encryption to protect the account credentials. For
technical and usability reasons only the ClientLogin google authentication method 
has been used (other would be AuthSub or OAuth). This method simply logs into
a google account providing its email and password - programmatically, on behalf 
of the user itself.

The problem with this approach is that the account credentials (email and
password) have to be saved in plain text, for the login process.

To (partially) solve this problem the module encrypts the credentials before
saving them in the database. This assures that the access to this data will be
granted only to whoever knows the «key» (that is Drupal itself, in that case).

To be able to encrypt & decrypt strings, the module uses the Encrypt module (and 
thus depends on it). Install and configure it before trying to access GDocs.

Read more about security provided by the Encrypt module here: http://drupal.org/project/encrypt

CREDITS
-------

Authored and maintained by Claudio Cicali <claudio.cicali@gmail.com>
Sponsored by Formez PA - http://www.formez.it


